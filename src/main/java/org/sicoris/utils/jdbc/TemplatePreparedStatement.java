/*
 * Copyright 2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.utils.jdbc;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplatePreparedStatement {
    private final static String PATTERN = "(\\$\\{(\\w)+\\})";

    private String templateSql;
    private String preparedStatementSql;

    private Map<String, ParameterEntry> parameters;

    private ParameterEntry getAndPutIfAbsent(String parameter) {
        ParameterEntry entry = parameters.get(parameter);
        if (entry == null) {
            entry = new ParameterEntry();
            parameters.put(parameter, entry);
        }

        return entry;
    }

    private void parseTemplate() {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(templateSql);
        int position = 1;
        while (matcher.find()) {
            String parameter = templateSql.substring(matcher.start() + 2, matcher.end() - 1);
            ParameterEntry entry = getAndPutIfAbsent(parameter);
            entry.addPosition(position);
            position++;
        }

        this.preparedStatementSql = matcher.replaceAll("?");
    }

    private ParameterEntry getParameterEntry(String parameter) throws ParameterNotDefinedException {
        ParameterEntry entry = this.parameters.get(parameter);
        if (entry == null) {
            throw new ParameterNotDefinedException(String.format("Parameter %s not defined", parameter));
        }
        return entry;
    }

    public TemplatePreparedStatement(String templateSql) throws SQLException {
        this.templateSql = templateSql;
        this.parameters = Maps.newHashMap();

        parseTemplate();
    }
    
    public String getPreparedSql() {
        return preparedStatementSql;
    }

    public boolean checkThatAllAttributesHaveBeenSet() {
        boolean result = true;
        for (ParameterEntry entry: parameters.values()) {
            result = result && entry.getInitialized();
        }

        return result;
    }



    public void setNull(PreparedStatement stmt, String parameter, int sqlType) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNull(position, sqlType);
        }

        entry.setInitialized(true);
    }

    public void setBoolean(PreparedStatement stmt, String parameter, boolean x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBoolean(position, x);
        }

        entry.setInitialized(true);
    }

    public void setByte(PreparedStatement stmt, String parameter, byte x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setByte(position, x);
        }

        entry.setInitialized(true);
    }

    public void setShort(PreparedStatement stmt, String parameter, short x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setShort(position, x);
        }

        entry.setInitialized(true);
    }

    public void setInt(PreparedStatement stmt, String parameter, int x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: getParameterEntry(parameter).getPositions()) {
            stmt.setInt(position, x);
        }

        entry.setInitialized(true);
    }

    public void setLong(PreparedStatement stmt, String parameter, long x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setLong(position, x);
        }

        entry.setInitialized(true);
    }

    public void setFloat(PreparedStatement stmt, String parameter, float x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setFloat(position, x);
        }

        entry.setInitialized(true);
    }

    public void setDouble(PreparedStatement stmt, String parameter, double x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setDouble(position, x);
        }

        entry.setInitialized(true);
    }

    public void setBigDecimal(PreparedStatement stmt, String parameter, BigDecimal x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBigDecimal(position, x);
        }

        entry.setInitialized(true);
    }

    public void setString(PreparedStatement stmt, String parameter, String x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setString(position, x);
        }

        entry.setInitialized(true);
    }

    public void setBytes(PreparedStatement stmt, String parameter, byte[] x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBytes(position, x);
        }

        entry.setInitialized(true);
    }

    public void setDate(PreparedStatement stmt, String parameter, Date x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setDate(position, x);
        }

        entry.setInitialized(true);
    }

    public void setTime(PreparedStatement stmt, String parameter, Time x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setTime(position, x);
        }

        entry.setInitialized(true);
    }

    public void setTimestamp(PreparedStatement stmt, String parameter, Timestamp x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setTimestamp(position, x);
        }

        entry.setInitialized(true);
    }

    public void setAsciiStream(PreparedStatement stmt, String parameter, InputStream x, int length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setAsciiStream(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setBinaryStream(PreparedStatement stmt, String parameter, InputStream x, int length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBinaryStream(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void clearParameters() throws SQLException {
        for (ParameterEntry entry: this.parameters.values()) {
            entry.setInitialized(false);
        }
    }

    public void setObject(PreparedStatement stmt, String parameter, Object x, int targetSqlType) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setObject(position, x, targetSqlType);
        }

        entry.setInitialized(true);
    }

    public void setObject(PreparedStatement stmt, String parameter, Object x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setObject(position, x);
        }

        entry.setInitialized(true);
    }

    public void setRef(PreparedStatement stmt, String parameter, Ref x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setRef(position, x);
        }

        entry.setInitialized(true);
    }

    public void setBlob(PreparedStatement stmt, String parameter, Blob x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBlob(position, x);
        }

        entry.setInitialized(true);
    }

    public void setClob(PreparedStatement stmt, String parameter, Clob x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setClob(position, x);
        }

        entry.setInitialized(true);
    }

    public void setArray(PreparedStatement stmt, String parameter, Array x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setArray(position, x);
        }

        entry.setInitialized(true);
    }

    public void setDate(PreparedStatement stmt, String parameter, Date x, Calendar cal) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setDate(position, x, cal);
        }

        entry.setInitialized(true);
    }

    public void setTime(PreparedStatement stmt, String parameter, Time x, Calendar cal) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setTime(position, x, cal);
        }

        entry.setInitialized(true);
    }

    public void setTimestamp(PreparedStatement stmt, String parameter, Timestamp x, Calendar cal) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setTimestamp(position, x, cal);
        }

        entry.setInitialized(true);
    }

    public void setNull(PreparedStatement stmt, String parameter, int sqlType, String typeName) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNull(position, sqlType, typeName);
        }

        entry.setInitialized(true);
    }

    public void setURL(PreparedStatement stmt, String parameter, URL x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setURL(position, x);
        }

        entry.setInitialized(true);
    }

    public void setRowId(PreparedStatement stmt, String parameter, RowId x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setRowId(position, x);
        }

        entry.setInitialized(true);
    }

    public void setNString(PreparedStatement stmt, String parameter, String value) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNString(position, value);
        }

        entry.setInitialized(true);
    }

    public void setNCharacterStream(PreparedStatement stmt, String parameter, Reader value, long length)
            throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNCharacterStream(position, value, length);
        }

        entry.setInitialized(true);
    }

    public void setNClob(PreparedStatement stmt, String parameter, NClob x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNClob(position, x);
        }

        entry.setInitialized(true);
    }

    public void setClob(PreparedStatement stmt, String parameter, Reader x, long length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setClob(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setBlob(PreparedStatement stmt, String parameter, InputStream x, long length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBlob(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setNClob(PreparedStatement stmt, String parameter, Reader x, long length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNClob(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setSQLXML(PreparedStatement stmt, String parameter, SQLXML x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setSQLXML(position, x);
        }

        entry.setInitialized(true);
    }

    public void setObject(PreparedStatement stmt, String parameter, Object x, int targetSqlType, int scaleOrLength)
            throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setObject(position, x, targetSqlType, scaleOrLength);
        }

        entry.setInitialized(true);
    }

    public void setAsciiStream(PreparedStatement stmt, String parameter, InputStream x, long length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setAsciiStream(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setBinaryStream(PreparedStatement stmt, String parameter, InputStream x, long length) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBinaryStream(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setCharacterStream(PreparedStatement stmt, String parameter, Reader x, long length)
            throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setCharacterStream(position, x, length);
        }

        entry.setInitialized(true);
    }

    public void setAsciiStream(PreparedStatement stmt, String parameter, InputStream x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setAsciiStream(position, x);
        }

        entry.setInitialized(true);
    }

    public void setBinaryStream(PreparedStatement stmt, String parameter, InputStream x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBinaryStream(position, x);
        }

        entry.setInitialized(true);
    }

    public void setCharacterStream(PreparedStatement stmt, String parameter, Reader x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setCharacterStream(position, x);
        }

        entry.setInitialized(true);
    }

    public void setNCharacterStream(PreparedStatement stmt, String parameter, Reader x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNCharacterStream(position, x);
        }

        entry.setInitialized(true);
    }

    public void setClob(PreparedStatement stmt, String parameter, Reader x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setClob(position, x);
        }

        entry.setInitialized(true);
    }

    public void setBlob(PreparedStatement stmt, String parameter, InputStream x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setBlob(position, x);
        }

        entry.setInitialized(true);
    }

    public void setNClob(PreparedStatement stmt, String parameter, Reader x) throws SQLException, ParameterNotDefinedException {
        ParameterEntry entry = getParameterEntry(parameter);
        for (Integer position: entry.getPositions()) {
            stmt.setNClob(position, x);
        }

        entry.setInitialized(true);
    }

    private static class ParameterEntry {
        boolean initialized = false;
        List<Integer> positions = Lists.newArrayList();

        public void setInitialized(boolean initialized) {
            this.initialized = initialized;
        }

        public boolean getInitialized() {
            return this.initialized;
        }

        public void addPosition(Integer position) {
            positions.add(position);
        }

        public List<Integer> getPositions() {
            return this.positions;
        }
    }
}
