/**
 * Copyright 2015 xavi.ferro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sicoris.utils.jdbc;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;

public class TemplatePreparedStatementTest {
    @Test
    public void testNoParameterRequired() throws Exception {
        String sql = "SELECT * FROM Test";
        new TemplatePreparedStatement(sql);
    }
    
    @Test ( expectedExceptions = ParameterNotDefinedException.class)
    public void testNoParameterRequiredButDefinedAnyway() throws Exception {
        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);

        String sql = "SELECT * FROM Test";
        new TemplatePreparedStatement(sql).setLong(stmt, "notDefined", 8);
    }

    @Test
    public void testAttributeHasNotBeenSet() throws Exception {
        String sql = "SELECT * FROM Test WHERE a < ${theA}";
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        Assert.assertFalse(builder.checkThatAllAttributesHaveBeenSet());
    }
    
    @Test
    public void testSettingNamedParameterThatDoesntExistWorks() throws Exception {
        String sql = "SELECT * FROM TEST where START_DATE < 2";

        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        Assert.assertEquals(builder.getPreparedSql(), sql);
    }

    @Test
    public void testClearParametersWork() throws Exception {
        String sql = "SELECT * FROM TEST where START_DATE < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);

        Assert.assertFalse(builder.checkThatAllAttributesHaveBeenSet());
        builder.setBoolean(stmt, "theValue", true);
        Assert.assertTrue(builder.checkThatAllAttributesHaveBeenSet());
        builder.clearParameters();
        Assert.assertFalse(builder.checkThatAllAttributesHaveBeenSet());
    }

    @Test
    public void testAValueIsSetMultipleTimes() throws Exception {
        String sql = "SELECT * FROM TEST where a < ${first} AND c = ${second} AND b > ${first}";
        String finalSql = "SELECT * FROM TEST where a < ? AND c = ? AND b > ?";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);

        builder.setBoolean(stmt, "first", true);
        builder.setInt(stmt, "second", 4);
        Assert.assertEquals(builder.getPreparedSql(), finalSql);
        Mockito.verify(stmt).setBoolean(1, true);
        Mockito.verify(stmt).setBoolean(3, true);
        Mockito.verify(stmt).setInt(2, 4);
    }

    @Test
    public void testSetNull() throws Exception {
        String sql = "SELECT * FROM TEST where var < ${theValue}";
        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);

        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNull(stmt, "theValue", 1);
        Mockito.verify(stmt).setNull(1, 1);
    }

    @Test
    public void testSetBoolean() throws Exception {
        String sql = "SELECT * FROM TEST where var < ${theValue}";
        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);

        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBoolean(stmt, "theValue", false);
        Mockito.verify(stmt).setBoolean(1, false);
    }

    @Test
    public void testSetByte() throws Exception {
        String sql = "SELECT * FROM TEST where var < ${theValue}";
        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);

        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setByte(stmt, "theValue", Byte.decode("0"));
        Mockito.verify(stmt).setByte(1, Byte.decode("0"));
    }

    @Test
    public void testSetShort() throws Exception {
        short x = 1;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setShort(stmt, "theValue", x);

        Mockito.verify(stmt).setShort(1, x);
    }

    @Test
    public void testSetInt() throws Exception {
        int x = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setInt(stmt, "theValue", x);

        Mockito.verify(stmt).setInt(1, x);
    }

    @Test
    public void testSetLong() throws Exception {
        long x = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setLong(stmt, "theValue", x);

        Mockito.verify(stmt).setLong(1, x);
    }

    @Test
    public void testSetFloat() throws Exception {
        float x = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setFloat(stmt, "theValue", x);

        Mockito.verify(stmt).setFloat(1, x);
    }

    @Test
    public void testSetDouble() throws Exception {
        double x = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";
        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setDouble(stmt, "theValue", x);

        Mockito.verify(stmt).setDouble(1, x);
    }

    @Test
    public void testSetBigDecimal() throws Exception {
        BigDecimal x = new BigDecimal(20);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBigDecimal(stmt, "theValue", x);

        Mockito.verify(stmt).setBigDecimal(1, x);
    }

    @Test
    public void testSetString() throws Exception {
        String x = "20";
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setString(stmt, "theValue", x);

        Mockito.verify(stmt).setString(1, x);
    }

    @Test
    public void testSetBytes() throws Exception {
        byte[] x = "20".getBytes();
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBytes(stmt, "theValue", x);

        Mockito.verify(stmt).setBytes(1, x);
    }

    @Test
    public void testSetDate() throws Exception {
        Date x = new Date(new java.util.Date().getTime());
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setDate(stmt, "theValue", x);

        Mockito.verify(stmt).setDate(1, x);
    }

    @Test
    public void testSetTime() throws Exception {
        Time x = new Time(new java.util.Date().getTime());
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setTime(stmt, "theValue", x);

        Mockito.verify(stmt).setTime(1, x);
    }

    @Test
    public void testSetTimestamp() throws Exception {
        Timestamp x = new Timestamp(new java.util.Date().getTime());
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setTimestamp(stmt, "theValue", x);

        Mockito.verify(stmt).setTimestamp(1, x);
    }

    @Test
    public void testSetAsciiStream() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        int length = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setAsciiStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setAsciiStream(1, x, length);
    }

    @Test
    public void testSetBinaryStreamWithIntLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        int length = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBinaryStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setBinaryStream(1, x, length);
    }

    @Test
    public void testSetBinaryStreamWithLongLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        long length = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBinaryStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setBinaryStream(1, x, length);
    }

    @Test
    public void testSetObjectWithSqlType() throws Exception {
        Object x = new String("TEST");
        int targetSqlType = 20;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setObject(stmt, "theValue", x, targetSqlType);

        Mockito.verify(stmt).setObject(1, x, targetSqlType);
    }

    @Test
    public void testSetObject() throws Exception {
        Object x = new String("TEST");
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setObject(stmt, "theValue", x);

        Mockito.verify(stmt).setObject(1, x);
    }

    @Test
    public void testSetRef() throws Exception {
        Ref x = Mockito.mock(Ref.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setRef(stmt, "theValue", x);

        Mockito.verify(stmt).setRef(1, x);
    }

    @Test
    public void testSetBlob() throws Exception {
        Blob x = Mockito.mock(Blob.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBlob(stmt, "theValue", x);

        Mockito.verify(stmt).setBlob(1, x);
    }

    @Test
    public void testSetClob() throws Exception {
        Clob x = Mockito.mock(Clob.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setClob(stmt, "theValue", x);

        Mockito.verify(stmt).setClob(1, x);
    }

    @Test
    public void testSetArray() throws Exception {
        Array x = Mockito.mock(Array.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setArray(stmt, "theValue", x);

        Mockito.verify(stmt).setArray(1, x);
    }

    @Test
    public void testSetDateWithCalendar() throws Exception {
        Date x = new Date(new java.util.Date().getTime());
        Calendar cal = Calendar.getInstance();
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setDate(stmt, "theValue", x, cal);

        Mockito.verify(stmt).setDate(1, x, cal);
    }

    @Test
    public void testSetTimeWithCalendar() throws Exception {
        Time x = new Time(new java.util.Date().getTime());
        Calendar cal = Calendar.getInstance();
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setTime(stmt, "theValue", x, cal);

        Mockito.verify(stmt).setTime(1, x, cal);
    }

    @Test
    public void testSetTimestampWithCalendar() throws Exception {
        Timestamp x = new Timestamp(new java.util.Date().getTime());
        Calendar cal = Calendar.getInstance();
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setTimestamp(stmt, "theValue", x, cal);

        Mockito.verify(stmt).setTimestamp(1, x, cal);
    }

    @Test
    public void testSetNullWithTypeName() throws Exception {
        int x = 12;
        String typeName = "TYPE_NAME";
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNull(stmt, "theValue", x, typeName);

        Mockito.verify(stmt).setNull(1, x, typeName);
    }

    @Test
    public void testSetRowId() throws Exception {
        RowId x = Mockito.mock(RowId.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setRowId(stmt, "theValue", x);

        Mockito.verify(stmt).setRowId(1, x);
    }

    @Test
    public void testSetNString() throws Exception {
        String x = "STRING";
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNString(stmt, "theValue", x);

        Mockito.verify(stmt).setNString(1, x);
    }

    @Test
    public void testSetNCharacterStream() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        int length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNCharacterStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setNCharacterStream(1, x, length);

    }

    @Test
    public void testSetNClob() throws Exception {
        NClob x = Mockito.mock(NClob.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNClob(stmt, "theValue", x);

        Mockito.verify(stmt).setNClob(1, x);
    }

    @Test
    public void testSetClobWithLength() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        long length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setClob(stmt, "theValue", x, length);

        Mockito.verify(stmt).setClob(1, x, length);
    }

    @Test
    public void testSetBlobWithLongLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        long length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBlob(stmt, "theValue", x, length);

        Mockito.verify(stmt).setBlob(1, x, length);
    }

    @Test
    public void testSetNClobWithReader() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        long length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNClob(stmt, "theValue", x, length);

        Mockito.verify(stmt).setNClob(1, x, length);
    }

    @Test
    public void testSetSQLXML() throws Exception {
        SQLXML x = Mockito.mock(SQLXML.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setSQLXML(stmt, "theValue", x);

        Mockito.verify(stmt).setSQLXML(1, x);
    }

    @Test
    public void testSetObjectWithTargetAndScale() throws Exception {
        Object x = Mockito.mock(Object.class);
        int targetSqlType = 23;
        int scaleOrLength = 46;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setObject(stmt, "theValue", x, targetSqlType, scaleOrLength);

        Mockito.verify(stmt).setObject(1, x, targetSqlType, scaleOrLength);
    }

    @Test
    public void testSetAsciiStreamWithIntLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        int length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setAsciiStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setAsciiStream(1, x, length);
    }

    @Test
    public void testSetURL() throws Exception {
        URL x = new URL("http://www.ebay.com");
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setURL(stmt, "theValue", x);

        Mockito.verify(stmt).setURL(1, x);
    }

    @Test
    public void testSetAsciiStreamWithLongLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        long length = 23L;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setAsciiStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setAsciiStream(1, x, length);
    }

    @Test
    public void testetBinaryStreamWithLength() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        int length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBinaryStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setBinaryStream(1, x, length);
    }

    @Test
    public void testSetCharacterStream() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        long length = 23;
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setCharacterStream(stmt, "theValue", x, length);

        Mockito.verify(stmt).setCharacterStream(1, x, length);
    }

    @Test
    public void testSetAsciiStreamWithInputStream() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setAsciiStream(stmt, "theValue", x);

        Mockito.verify(stmt).setAsciiStream(1, x);
    }

    @Test
    public void testSetBinaryStreamWithInputStream() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBinaryStream(stmt, "theValue", x);

        Mockito.verify(stmt).setBinaryStream(1, x);
    }

    @Test
    public void testSetCharacterStreamWithReader() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setCharacterStream(stmt, "theValue", x);

        Mockito.verify(stmt).setCharacterStream(1, x);
    }

    @Test
    public void setNCharacterStream() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNCharacterStream(stmt, "theValue", x);

        Mockito.verify(stmt).setNCharacterStream(1, x);
    }

    @Test
    public void testSetClobWithReader() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setClob(stmt, "theValue", x);

        Mockito.verify(stmt).setClob(1, x);
    }

    @Test
    public void testSetBlobWithInputStream() throws Exception {
        InputStream x = Mockito.mock(InputStream.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setBlob(stmt, "theValue", x);

        Mockito.verify(stmt).setBlob(1, x);
    }

    @Test
    public void testSetNClobWithReader2() throws Exception {
        Reader x = Mockito.mock(Reader.class);
        String sql = "SELECT * FROM TEST where var < ${theValue}";

        PreparedStatement stmt = Mockito.mock(PreparedStatement.class);
        TemplatePreparedStatement builder = new TemplatePreparedStatement(sql);
        builder.setNClob(stmt, "theValue", x);

        Mockito.verify(stmt).setNClob(1, x);
    }
}
